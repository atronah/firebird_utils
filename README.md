**EN** | [RU][]

Moved to [GitHub](https://github.com/atronah/firebird_utils)
=================

Firebird utils
==============
Project for useful things (scripts, queries, udf-librarires) for Firebird.


Contents
--------
* [UDF](udf/README.md) - Libraries of user defined functions to use from Firebird directly.

[RU]: README_ru.md